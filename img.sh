#!/bin/bash

if [ $PLATFORM != rpi4 ]; then
    echo "unsupported platform"
    exit -1
fi

if [ ! $ARCH ]; then
    export ARCH=$(uname -m)
fi

IMG_NAME=EchoOS-$ARCH-$PLATFORM.img
rm -f $IMG_NAME

BOOT_SIZE_MB=32
TOTAL_SIZE_MB=2048

dd if=/dev/zero of=$IMG_NAME bs=1 count=0 seek=$((TOTAL_SIZE_MB * 1024 * 1024))

/sbin/parted $IMG_NAME mklabel msdos
/sbin/parted $IMG_NAME mkpart primary fat32 1MiB $((BOOT_SIZE_MB + 1))
/sbin/parted $IMG_NAME mkpart primary ext4 $((BOOT_SIZE_MB + 1))MiB 100%

LOOP_BOOT=$(/sbin/losetup -f --show -o 1048576 $IMG_NAME)
LOOP_ROOTFS=$(/sbin/losetup -f --show -o $((1048576 + BOOT_SIZE_MB * 1024 * 1024)) $IMG_NAME)

mkdir tmp tmp/boot tmp/rootfs

/sbin/mkfs.vfat -F 32 -n BOOT $LOOP_BOOT
/sbin/mkfs.ext4 -L rootfs $LOOP_ROOTFS

mount $LOOP_BOOT tmp/boot
mount $LOOP_ROOTFS tmp/rootfs

cp -r rootfs/boot/* tmp/boot
cp -r rootfs/* tmp/rootfs

umount tmp/boot tmp/rootfs
/sbin/losetup -d $LOOP_BOOT $LOOP_ROOTFS

chmod 777 -R $IMG_NAME tmp
