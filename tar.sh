#!/bin/bash

if [ ! $ARCH ]; then
    export ARCH=$(uname -m)
fi

cd rootfs
tar -czvf ../EchoOS-$ARCH.tar.gz .
