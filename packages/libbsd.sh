#!/bin/bash

export CC=clang
export AR=llvm-ar
export AS=llvm-as
export LD=ld.lld
export RANLIB=llvm-ranlib
export READELF=llvm-readelf
export STRIP=llvm-strip
export PKG_CONFIG_PATH=$ROOTFS/lib/pkgconfig

cd $TMPDIR
tar -xvf $SRCDIR/libbsd-$LIBBSD_VERSION.tar.xz
cd libbsd-$LIBBSD_VERSION
patch -p1 < $ROOT/patches/libbsd/off64_t.patch
cd ..
mkdir build
cd build
../libbsd-$LIBBSD_VERSION/configure --host=$ARCH-linux-musl --prefix=/
make -j$THREADS
make install DESTDIR=$ROOTFS

rm -rf $ROOTFS/share/man
