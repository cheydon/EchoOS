#!/bin/bash

export CC=clang
export AR=llvm-ar
export AS=llvm-as
export LD=ld.lld
export RANLIB=llvm-ranlib
export READELF=llvm-readelf
export STRIP=llvm-strip
export CFLAGS="-target $ARCH-linux-musl -fuse-ld=lld"
export LIBCC="-rtlib=compiler-rt -L$(llvm-config --libdir)/clang/$(ls $(llvm-config --libdir)/clang)/lib/linux -lclang_rt.builtins-$ARCH"

cd $TMPDIR
tar -xvf $SRCDIR/musl-$MUSL_VERSION.tar.gz
mkdir build
cd build
../musl-$MUSL_VERSION/configure --host=$ARCH-linux-musl --prefix=/ --disable-wrapper
make -j$THREADS
make install DESTDIR=$ROOTFS
mv $ROOTFS/lib/ld-musl-$ARCH.so.1 $ROOTFS/lib/ld-echo-$ARCH.so.1
ln -s /lib/ld-echo-$ARCH.so.1 $ROOTFS/bin/ldd
