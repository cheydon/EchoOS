#!/bin/bash

export LLVM=1
export CROSS_COMPILE=$ARCH-linux-musl-
export LINUX_ARCH=$ARCH

if [ $ARCH == aarch64 ]; then
    export LINUX_ARCH=arm64
elif [ $ARCH == riscv64 ]; then
    export LINUX_ARCH=riscv
fi

cd $TMPDIR
tar -xvf $SRCDIR/linux-$LINUX_VERSION.tar.xz
cd linux-$LINUX_VERSION
ARCH=$LINUX_ARCH make defconfig

if [ $ARCH == i386 ]; then
    sed -i "/# CONFIG_M486 is not set/d" .config
    echo "CONFIG_M486=y" >> .config
    sed -i "s/CONFIG_M686=y/CONFIG_M686=n/" .config
    sed -i "s/CONFIG_X86_INTERNODE_CACHE_SHIFT=5/CONFIG_X86_INTERNODE_CACHE_SHIFT=4/" .config
    sed -i "s/CONFIG_X86_L1_CACHE_SHIFT=5/CONFIG_X86_L1_CACHE_SHIFT=4/" .config
    sed -i "s/CONFIG_X86_USE_PPRO_CHECKSUM=y/# CONFIG_X86_USE_PPRO_CHECKSUM is not set/" .config
    sed -i "s/CONFIG_X86_TSC=y/# CONFIG_X86_TSC is not set/" .config
    sed -i "s/CONFIG_X86_CMPXCHG64=y/# CONFIG_X86_CMPXCHG64 is not set/" .config
    sed -i "s/CONFIG_X86_CMOV=y/# CONFIG_X86_CMOV is not set/" .config
    sed -i "s/CONFIG_X86_MINIMUM_CPU_FAMILY=6/CONFIG_X86_MINIMUM_CPU_FAMILY=4/" .config
    sed -i "s/CONFIG_X86_DEBUGCTLMSR=y/# CONFIG_X86_DEBUGCTLMSR is not set/" .config
    echo "CONFIG_X86_F00F_BUG=y" >> .config
    echo "CONFIG_X86_INVD_BUG=y" >> .config
    echo "CONFIG_X86_ALIGNMENT_16=y" >> .config
    echo "CONFIG_CPU_SUP_CYRIX_32=y" >> .config
    echo "CONFIG_CPU_SUP_UMC_32=y" >> .config
    sed -i "s/CONFIG_FUNCTION_PADDING_CFI=0/CONFIG_FUNCTION_PADDING_CFI=11/" .config
    sed -i "s/CONFIG_FUNCTION_PADDING_BYTES=4/CONFIG_FUNCTION_PADDING_BYTES=16/" .config
    sed -i "s/CONFIG_FUNCTION_ALIGNMENT=4/CONFIG_FUNCTION_ALIGNMENT=16/" .config
    echo "CONFIG_FUNCTION_ALIGNMENT_16B=y" >. .config
fi

if [ $ARCH == i386 ] || [ $ARCH == x86_64 ]; then
    sed -i "/# CONFIG_FB is not set/d" .config
    echo "CONFIG_FB=y" >> .config
    echo "CONFIG_SWITCHEROO=y" >> .config
    echo "CONFIG_DRM_FBDEV_EMULATION=n" >> .config
    echo "CONFIG_FIRMWARE_EDID=n" >> .config
    echo "CONFIG_FB_FOREIGN_ENDIAN=n" >> .config
    echo "CONFIG_FB_MODE_HELPERS=n" >> .config
    echo "CONFIG_FB_TILEBLITTING=n" >> .config
    echo "CONFIG_FB_CIRRUS=n" >> .config
    echo "CONFIG_FB_PM2=n" >> .config
    echo "CONFIG_FB_CYBER2000=n" >> .config
    echo "CONFIG_FB_ARC=n" >> .config
    echo "CONFIG_FB_ASILIANT=n" >> .config
    echo "CONFIG_FB_IMSTT=n" >> .config
    echo "CONFIG_FB_VGA16=y" >> .config
    echo "CONFIG_FB_UVESA=n" >> .config
    echo "CONFIG_FB_VESA=y" >> .config
    echo "CONFIG_FB_EFI=y" >> .config
    echo "CONFIG_FB_N411=n" >> .config
    echo "CONFIG_FB_HGA=n" >> .config
    echo "CONFIG_FB_OPENCORES=n" >> .config
    echo "CONFIG_FB_S1D13XXX=n" >> .config
    echo "CONFIG_FB_NVIDIA=n" >> .config
    echo "CONFIG_FB_RIVA=n" >> .config
    echo "CONFIG_FB_I740=n" >> .config
    echo "CONFIG_FB_LE80578=n" >> .config
    echo "CONFIG_FB_MATROX=n" >> .config
    echo "CONFIG_FB_RADEON=n" >> .config
    echo "CONFIG_FB_ATY128=n" >> .config
    echo "CONFIG_FB_ATY=n" >> .config
    echo "CONFIG_FB_S3=n" >> .config
    echo "CONFIG_FB_SAVAGE=n" >> .config
    echo "CONFIG_FB_SIS=n" >> .config
    echo "CONFIG_FB_NEOMAGIC=n" >> .config
    echo "CONFIG_FB_KYRO=n" >> .config
    echo "CONFIG_FB_3DFX=n" >> .config
    echo "CONFIG_FB_VOODOO1=n" >> .config
    echo "CONFIG_FB_VT8623=n" >> .config
    echo "CONFIG_FB_TRIDENT=n" >> .config
    echo "CONFIG_FB_ARK=n" >> .config
    echo "CONFIG_FB_PM3=n" >> .config
    echo "CONFIG_FB_CARMINE=n" >> .config
    echo "CONFIG_FB_SMSCUFX=n" >> .config
    echo "CONFIG_FB_UDL=n" >> .config
    echo "CONFIG_FB_IBM_GXT4500=n" >> .config
    echo "CONFIG_FB_VIRTUAL=n" >> .config
    echo "CONFIG_FB_METRONOME=n" >> .config
    echo "CONFIG_FB_MB862XX=n" >> .config
    echo "CONFIG_FB_SIMPLE=n" >> .config
    echo "CONFIG_FB_SM712=n" >> .config
    echo "CONFIG_FRAMEBUFFER_CONSOLE=y" >> .config
    echo "CONFIG_FRAMEBUFFER_CONSOLE_LEGACY_ACCELERATION=n" >> .config
    echo "CONFIG_FRAMEBUFFER_CONSOLE_DETECT_PRIMARY=n" >> .config
    echo "CONFIG_FRAMEBUFFER_CONSOLE_ROTATION=n" >> .config
    echo "CONFIG_FRAMEBUFFER_CONSOLE_DEFERRED_TAKEOVER=n" >> .config
    echo "CONFIG_LOGO=n" >> .config
    echo "CONFIG_FONTS=n" >> .config
    echo "CONFIG_FB_I810=n" >> .config
    echo "CONFIG_FB_GEODE=n" >> .config
    echo "CONFIG_FB_DEVICE=y" >> .config
fi

sed -i "/# CONFIG_UEVENT_HELPER is not set/d" .config
echo "CONFIG_UEVENT_HELPER=y" >> .config
echo 'CONFIG_UEVENT_HELPER_PATH="/sbin/mdev"' >> .config

ARCH=$LINUX_ARCH make headers_install INSTALL_HDR_PATH=$ROOTFS
ARCH=$LINUX_ARCH make -j$THREADS

if [ $ARCH == i386 ] || [ $ARCH == x86_64 ]; then
    cp arch/x86/boot/bzImage $ROOTFS/boot
elif [ $ARCH == aarch64 ] || [ $ARCH == riscv64 ]; then
    cp arch/$LINUX_ARCH/boot/Image.gz $ROOTFS/boot
fi

if [ $PLATFORM == rpi4 ]; then
    cp arch/arm/boot/dts/broadcom/bcm2711-rpi-4-b.dts $ROOTFS/boot
    cp $SRCDIR/rpi-firmware/boot/bootcode.bin $ROOTFS/boot
    cp $SRCDIR/rpi-firmware/boot/start.elf $ROOTFS/boot
    cp $SRCDIR/rpi-firmware/boot/fixup.dat $ROOTFS/boot
    cp -r $ROOT/overlays/rpi4/* $ROOTFS
fi

ARCH=$LINUX_ARCH make modules_install INSTALL_MOD_PATH=$ROOTFS
rm -f $ROOTFS/lib/modules/$LINUX_VERSION/build $ROOTFS/lib/modules/$LINUX_VERSION/source
