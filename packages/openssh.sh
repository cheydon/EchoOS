#!/bin/bash

export CC=clang
export AR=llvm-ar
export AS=llvm-as
export LD=ld.lld
export RANLIB=llvm-ranlib
export READELF=llvm-readelf
export STRIP=llvm-strip
export PKG_CONFIG_PATH=$ROOTFS/lib/pkgconfig

cd $TMPDIR
tar -xvf $SRCDIR/openssh-$OPENSSH_VERSION.tar.gz
mkdir build
cd build
../openssh-$OPENSSH_VERSION/configure --host=$ARCH-linux-musl --prefix=/ --disable-strip --libexecdir=/sbin
make -j$THREADS
make install-nokeys DESTDIR=$ROOTFS

rm -rf $ROOTFS/share/man
