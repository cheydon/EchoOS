#!/bin/bash

export CFLAGS=-static

cd $TMPDIR
tar -xvf $SRCDIR/busybox-$BUSYBOX_VERSION.tar.bz2
cd busybox-$BUSYBOX_VERSION
patch -p1 < $ROOT/patches/busybox/llvm.patch
patch -p1 < $ROOT/patches/busybox/no_const_ptr_to_globals.patch
make defconfig
sed -i "s/CONFIG_DPKG=y/CONFIG_DPKG=n/" .config
sed -i "s/CONFIG_DPKG_DEB=y/CONFIG_DPKG_DEB=n/" .config
sed -i "s/CONFIG_RPM=y/CONFIG_RPM=n/" .config
sed -i "s/CONFIG_RPM2CPIO=y/CONFIG_RPM2CPIO=n/" .config
sed -i "s/CONFIG_LINUXRC=y/CONFIG_LINUXRC=n/" .config
sed -i 's#CONFIG_UNAME_OSNAME="GNU/Linux"#CONFIG_UNAME_OSNAME="EchoOS"#' .config
sed -i "s/CONFIG_STATIC_LIBGCC=y/CONFIG_STATIC_LIBGCC=n/" .config
sed -i "s/CONFIG_FTPGET=y/CONFIG_FTPGET=n/" .config
sed -i "s/CONFIG_FTPPUT=y/CONFIG_FTPPUT=n/" .config
sed -i "s/CONFIG_FEATURE_FTPGETPUT_LONG_OPTIONS=y/CONFIG_FEATURE_FTPGETPUT_LONG_OPTIONS=n/" .config
make -j$THREADS
make install CONFIG_PREFIX=$ROOTFS

mkdir $ROOTFS/share/udhcpc
cp examples/udhcp/simple.script $ROOTFS/share/udhcpc/default.script
chmod +x $ROOTFS/share/udhcpc/default.script
