#!/bin/bash

if [ $PLATFORM ]; then
    echo "unsupported platform"
    exit -1
fi

if [ ! $ARCH ]; then
    export ARCH=$(uname -m)
fi

source ./versions.sh

mkdir tmp
cd tmp
rm -rf limine
cp -r ../sources/limine .
cd limine
make
mkdir -p ../../rootfs/boot/limine
cp limine-bios.sys ../../rootfs/boot/limine
cp limine-bios-cd.bin limine-uefi-cd.bin ../../rootfs
mkdir -p ../../rootfs/EFI/BOOT
cp BOOTX64.EFI BOOTIA32.EFI ../../rootfs/EFI/BOOT
cd ../../

cp -r overlays/iso/* rootfs
xorriso -as mkisofs -b limine-bios-cd.bin -no-emul-boot -boot-load-size 4 -boot-info-table --efi-boot limine-uefi-cd.bin -efi-boot-part --efi-boot-image --protective-msdos-label rootfs -o EchoOS-$ARCH.iso
tmp/limine/limine bios-install EchoOS-$ARCH.iso
