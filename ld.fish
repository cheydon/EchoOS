#!/bin/fish

set ARCH $argv[-3]
set ROOTFS $argv[-2]
set STATIC_ARGS
set SHARED
set LIBRARIES -lc
set OBJS
set LD_ARGS
set STDLIB $argv[-1]
set TOOLCHAINDIR $ROOTFS/../toolchain
set STARTUP_CRT1 crt1.o

if test $STDLIB = nostdlib
    set LIBRARIES
end

set IGNORE_NEXT

for ARG in $argv
    if test $IGNORE_NEXT
        set IGNORE_NEXT
        continue
    else if test $ARG = $ARCH
        break
    else if test $ARG = -static
        set STATIC_ARGS -static
    else if test $ARG = -shared
        set SHARED 1
    else if test $ARG = -lgcc; or test $ARG = -lgcc_s; or test $ARG = -lgcc_eh; or test $ARCH = -static-libgcc
    else if test $ARG = -pie
        set STARTUP_CRT1 Scrt1.o
        set LD_ARGS $LD_ARGS $ARG
    else if test $ARG = -dynamic-linker
        set IGNORE_NEXT 1
    else
        set LD_ARGS $LD_ARGS $ARG
    end
end

if not test $STDLIB = nostdlib
    set OBJS "$ROOTFS/lib/crti.o $ROOTFS/lib/$STARTUP_CRT1 $ROOTFS/lib/crtn.o"
end

if not test $STATIC_ARGS
    set STATIC_ARGS -dynamic-linker /lib/ld-echo-$ARCH.so.1
end

set LLVM_CONFIG llvm-config

if test -d $TOOLCHAINDIR
    set LLVM_CONFIG $TOOLCHAINDIR/bin/llvm-config
end

set BUILTINS_ARGS ($LLVM_CONFIG --libdir)/clang/(ls (llvm-config --libdir)/clang)/lib/linux/libclang_rt.builtins-$ARCH.a

if test $SHARED
    /bin/sh -c "ld.lld $LD_ARGS -shared $LIBRARIES $BUILTINS_ARGS"
else
    /bin/sh -c "ld.lld $STATIC_ARGS $LD_ARGS $OBJS $LIBRARIES $BUILTINS_ARGS"
end
