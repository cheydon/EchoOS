#!/bin/fish

set ROOT (pwd)
set ARCH (uname -m)

source ./versions.sh

set SRCDIR (pwd)/sources
set TOOLCHAINDIR (pwd)/toolchain

rm -rf $TOOLCHAINDIR
mkdir $TOOLCHAINDIR
cd $TOOLCHAINDIR

tar -xvf $SRCDIR/llvmorg-$LLVM_VERSION.tar.gz
cd llvm-project-llvmorg-$LLVM_VERSION
cmake -Bbuild -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$TOOLCHAINDIR -DLLVM_ENABLE_PROJECTS="llvm;clang;lld" -DLLVM_TARGETS_TO_BUILD="X86;ARM;AArch64;RISCV" llvm
cmake --build build --parallel (nproc)
cmake --build build --target install
rm -rf build

set PATH $TOOLCHAINDIR/bin:$PATH

function build_compiler_rt
    set COMPILER_RT_ARCH $argv[1]

    if test $COMPILER_RT_ARCH = $ARCH
        set GCC_FLAGS "--sysroot=/ -static"
    else
        if test $COMPILER_RT_ARCH = i386
            set SYSROOT_PATH /usr/i686-linux-gnu
            set GCC_PATH (i686-linux-gnu-gcc -print-search-dirs | awk '/install:/{print $2; exit}')
        else
            set SYSROOT_PATH /usr/$COMPILER_RT_ARCH-linux-gnu
            set GCC_PATH ($COMPILER_RT_ARCH-linux-gnu-gcc -print-search-dirs | awk '/install:/{print $2; exit}')
        end

        rm -rf $TOOLCHAINDIR/sysroot
        mkdir $TOOLCHAINDIR/sysroot
        cp -r $SYSROOT_PATH/* $TOOLCHAINDIR/sysroot
        cp -r $GCC_PATH/* $TOOLCHAINDIR/sysroot/lib

        set GCC_FLAGS "--sysroot=$TOOLCHAINDIR/sysroot -static"
    end

    cmake -Bbuild -DCMAKE_AR=(which llvm-ar) -DCMAKE_ASM_COMPILER_TARGET=$COMPILER_RT_ARCH-linux-musl -DCMAKE_ASM_FLAGS=$GCC_FLAGS -DCMAKE_C_COMPILER=clang -DCMAKE_C_COMPILER_TARGET=$COMPILER_RT_ARCH-linux-musl -DCMAKE_C_FLAGS=$GCC_FLAGS -DCMAKE_EXE_LINKER_FLAGS=-fuse-ld=lld -DCMAKE_NM=(which llvm-nm) -DCMAKE_RANLIB=(which llvm-ranlib) -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$TOOLCHAINDIR -DCOMPILER_RT_BUILD_BUILTINS=Yes -DCOMPILER_RT_BUILD_LIBFUZZER=No -DCOMPILER_RT_BUILD_MEMPROF=No -DCOMPILER_RT_BUILD_PROFILE=No -DCOMPILER_RT_BUILD_SANITIZERS=No -DCOMPILER_RT_BUILD_XRAY=No -DCOMPILER_RT_DEFAULT_TARGET_ONLY=Yes compiler-rt
    cmake --build build --parallel (nproc)
    cmake --build build --target install
    rm -rf build
    rm -rf $TOOLCHAINDIR/sysroot
end

build_compiler_rt i386
build_compiler_rt x86_64
build_compiler_rt aarch64
build_compiler_rt riscv64

ln -s $TOOLCHAINDIR/lib $TOOLCHAINDIR/lib/clang/(llvm-config --version | cut -d. -f1)/lib
