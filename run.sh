#!/bin/bash

if [ ! $ARCH ]; then
    export ARCH=$(uname -m)
fi

qemu-system-$ARCH -cdrom EchoOS-$ARCH.iso
