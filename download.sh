#!/bin/bash

source ./versions.sh

rm -rf sources
mkdir sources
cd sources

wget https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-$LINUX_VERSION.tar.xz
wget https://musl.libc.org/releases/musl-$MUSL_VERSION.tar.gz
wget https://busybox.net/downloads/busybox-$BUSYBOX_VERSION.tar.bz2
git clone https://github.com/limine-bootloader/limine.git --branch v$LIMINE_VERSION-binary --depth 1
wget https://github.com/llvm/llvm-project/archive/refs/tags/llvmorg-$LLVM_VERSION.tar.gz
wget https://libbsd.freedesktop.org/releases/libmd-$LIBMD_VERSION.tar.xz
wget https://libbsd.freedesktop.org/releases/libbsd-$LIBBSD_VERSION.tar.xz
wget https://ftp.openbsd.org/pub/OpenBSD/LibreSSL/libressl-$LIBRESSL_VERSION.tar.gz
wget https://www.zlib.net/zlib-$ZLIB_VERSION.tar.gz
wget https://ftp.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-$OPENSSH_VERSION.tar.gz
git clone https://github.com/raspberrypi/firmware.git --branch $RPI_FIRMWARE_VERSION --depth 1 rpi-firmware
