#!/bin/bash

export ROOT=$(pwd)

if [ ! $ARCH ]; then
    export ARCH=$(uname -m)
fi

source ./versions.sh

export SRCDIR=$ROOT/sources
export THREADS=$(nproc)
export ROOTFS=$ROOT/rootfs
export TOOLCHAINDIR=$ROOT/toolchain

rm -rf tmp
mkdir tmp

rm -rf $ROOTFS
mkdir $ROOTFS $ROOTFS/include $ROOTFS/boot $ROOTFS/bin $ROOTFS/sbin $ROOTFS/dev $ROOTFS/sys $ROOTFS/lib $ROOTFS/etc $ROOTFS/proc $ROOTFS/tmp $ROOTFS/var $ROOTFS/var/lib $ROOTFS/var/log $ROOTFS/var/empty $ROOTFS/share $ROOTFS/root $ROOTFS/mnt
ln -s . $ROOTFS/usr
ln -s /proc/mounts $ROOTFS/etc/fstab
cp -r overlays/rootfs/* $ROOTFS

export PATH=$TOOLCHAINDIR/bin:$PATH

mkdir tmp/linux
TMPDIR=$ROOT/tmp/linux packages/linux.sh

mkdir tmp/musl
TMPDIR=$ROOT/tmp/musl packages/musl.sh

export PATH=$ROOT:$PATH

mkdir tmp/busybox
TMPDIR=$ROOT/tmp/busybox packages/busybox.sh

mkdir tmp/libmd
TMPDIR=$ROOT/tmp/libmd packages/libmd.sh

mkdir tmp/libbsd
TMPDIR=$ROOT/tmp/libbsd packages/libbsd.sh

mkdir tmp/libressl
TMPDIR=$ROOT/tmp/libressl packages/libressl.sh

mkdir tmp/zlib
TMPDIR=$ROOT/tmp/zlib packages/zlib.sh

mkdir tmp/openssh
TMPDIR=$ROOT/tmp/openssh packages/openssh.sh
